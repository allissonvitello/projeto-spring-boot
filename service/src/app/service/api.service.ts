import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Contato} from "../model/contato.model";
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api.response";

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:8080/contatos/';

  getContatos() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl);
  }

  getContatoById(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + id);
  }

  createContato(contato: Contato): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl, contato);
  }

  updateContato(contato: Contato): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + contato.id, contato);
  }

  deleteContato(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + id);
  }
}
