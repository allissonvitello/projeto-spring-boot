import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {Contato} from "../../model/contato.model";
import {ApiService} from "../../service/api.service";

@Component({
  selector: 'app-edit-contato',
  templateUrl: './edit-contato.component.html',
  styleUrls: ['./edit-contato.component.css']
})
export class EditContatoComponent implements OnInit {

  contato: Contato;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    let ContatoId = window.localStorage.getItem("editContatoId");
    if(!ContatoId) {
      alert("Invalid action.")
      this.router.navigate(['list-contato']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      nome: ['', Validators.required],
      email: ['', Validators.required],
    });
    this.apiService.getContatoById(+ContatoId)
      .subscribe( data => {
        this.editForm.setValue(data);
      });
  }

  onSubmit() {
    this.apiService.updateContato(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          if(data.status === 200) {
            alert('Contato updated successfully.');
            this.router.navigate(['list-contato']);
          }else {
            alert(data.message);
          }
        },
        error => {
          alert(error);
        });
  }

}
