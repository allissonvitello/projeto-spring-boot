import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {Contato} from "../../model/contato.model";
import {ApiService} from "../../service/api.service";

@Component({
  selector: 'app-list-contato',
  templateUrl: './list-contato.component.html',
  styleUrls: ['./list-contato.component.css']
})
export class ListContatoComponent implements OnInit {

  contatos: Contato[] = [];

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getContatos()
      .subscribe( data => {
        this.contatos = data;
      },
      error => {
        console.error(error);
    });
  }

  deleteContato(contato: Contato): void {
    this.apiService.deleteContato(contato.id)
      .subscribe( data => {
        this.contatos = this.contatos.filter(u => u !== contato);
      })
  };

  editContato(contato: Contato): void {
    window.localStorage.removeItem("editContatoId");
    window.localStorage.setItem("editContatoId", contato.id.toString());
    this.router.navigate(['edit-contato']);
  };

  addContato(): void {
    this.router.navigate(['add-contato']);
  };
}
