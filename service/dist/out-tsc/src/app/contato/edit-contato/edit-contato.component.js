import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Validators } from "@angular/forms";
import { first } from "rxjs/operators";
let EditContatoComponent = class EditContatoComponent {
    constructor(formBuilder, router, apiService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.apiService = apiService;
    }
    ngOnInit() {
        let userId = window.localStorage.getItem("editContatoId");
        if (!userId) {
            alert("Invalid action.");
            this.router.navigate(['list-contato']);
            return;
        }
        this.editForm = this.formBuilder.group({
            id: [''],
            username: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            age: ['', Validators.required],
            salary: ['', Validators.required]
        });
        this.apiService.getContatoById(+userId)
            .subscribe(data => {
            this.editForm.setValue(data.result);
        });
    }
    onSubmit() {
        this.apiService.updateContato(this.editForm.value)
            .pipe(first())
            .subscribe(data => {
            if (data.status === 200) {
                alert('Contato updated successfully.');
                this.router.navigate(['list-contato']);
            }
            else {
                alert(data.message);
            }
        }, error => {
            alert(error);
        });
    }
};
EditContatoComponent = tslib_1.__decorate([
    Component({
        selector: 'app-edit-contato',
        templateUrl: './edit-contato.component.html',
        styleUrls: ['./edit-contato.component.css']
    })
], EditContatoComponent);
export { EditContatoComponent };
//# sourceMappingURL=edit-contato.component.js.map