import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let ListContatoComponent = class ListContatoComponent {
    constructor(router, apiService) {
        this.router = router;
        this.apiService = apiService;
    }
    ngOnInit() {
        if (!window.localStorage.getItem('token')) {
            this.router.navigate(['login']);
            return;
        }
        this.apiService.getContatos()
            .subscribe(data => {
            this.contatos = data.result;
        });
    }
    deleteContato(contato) {
        this.apiService.deleteContato(contato.id)
            .subscribe(data => {
            this.contatos = this.contatos.filter(u => u !== contato);
        });
    }
    ;
    editContato(contato) {
        window.localStorage.removeItem("editContatoId");
        window.localStorage.setItem("editContatoId", contato.id.toString());
        this.router.navigate(['edit-contato']);
    }
    ;
    addContato() {
        this.router.navigate(['add-contato']);
    }
    ;
};
ListContatoComponent = tslib_1.__decorate([
    Component({
        selector: 'app-list-contato',
        templateUrl: './list-contato.component.html',
        styleUrls: ['./list-contato.component.css']
    })
], ListContatoComponent);
export { ListContatoComponent };
//# sourceMappingURL=list-contato.component.js.map