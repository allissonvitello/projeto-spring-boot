import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Validators } from "@angular/forms";
let AddContatoComponent = class AddContatoComponent {
    constructor(formBuilder, router, apiService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.apiService = apiService;
    }
    ngOnInit() {
        this.addForm = this.formBuilder.group({
            id: [],
            username: ['', Validators.required],
            password: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            age: ['', Validators.required],
            salary: ['', Validators.required]
        });
    }
    onSubmit() {
        this.apiService.createContato(this.addForm.value)
            .subscribe(data => {
            this.router.navigate(['list-contato']);
        });
    }
};
AddContatoComponent = tslib_1.__decorate([
    Component({
        selector: 'app-add-contato',
        templateUrl: './add-contato.component.html',
        styleUrls: ['./add-contato.component.css']
    })
], AddContatoComponent);
export { AddContatoComponent };
//# sourceMappingURL=add-contato.component.js.map