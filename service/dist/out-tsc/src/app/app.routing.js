import { RouterModule } from '@angular/router';
import { AddContatoComponent } from "./contato/add-contato/add-contato.component";
import { ListContatoComponent } from "./contato/list-contato/list-contato.component";
import { EditContatoComponent } from "./contato/edit-contato/edit-contato.component";
const routes = [
    { path: 'add-contato', component: AddContatoComponent },
    { path: 'list-contato', component: ListContatoComponent },
    { path: 'edit-contato', component: EditContatoComponent }
];
export const routing = RouterModule.forRoot(routes);
//# sourceMappingURL=app.routing.js.map