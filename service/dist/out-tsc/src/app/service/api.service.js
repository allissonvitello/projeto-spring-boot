import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
let ApiService = class ApiService {
    constructor(http) {
        this.http = http;
        this.baseUrl = 'http://localhost:8080/contatos/';
    }
    getContatos() {
        return this.http.get(this.baseUrl);
    }
    getContatoById(id) {
        return this.http.get(this.baseUrl + id);
    }
    createContato(contato) {
        return this.http.post(this.baseUrl, contato);
    }
    updateContato(contato) {
        return this.http.put(this.baseUrl + contato.id, contato);
    }
    deleteContato(id) {
        return this.http.delete(this.baseUrl + id);
    }
};
ApiService = tslib_1.__decorate([
    Injectable()
], ApiService);
export { ApiService };
//# sourceMappingURL=api.service.js.map