import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
let TokenInterceptor = class TokenInterceptor {
    intercept(request, next) {
        let token = window.localStorage.getItem('token');
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token
                }
            });
        }
        return next.handle(request);
    }
};
TokenInterceptor = tslib_1.__decorate([
    Injectable()
], TokenInterceptor);
export { TokenInterceptor };
//# sourceMappingURL=interceptor.js.map